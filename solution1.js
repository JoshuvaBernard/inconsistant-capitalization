/* 
  array - sorted
*/

// To find the time take to be executed
const start = Date.now();

// To extract text from the html page
const JSSoup = require("jssoup").default;

const fs = require("fs");

function caseCheck(first, second) {
  /*
    To check if the two words have inconsistant capitalization
    Args:
      *first(string), second(string) - two words that need to be compared
    Returns:
      * 1 - if there is no inconsistant capitalization
      * 2 - if there is inconsistant capitalization
      * 0 - if the words are different
  */
  if (first === second) {
    return 1;
  } else if (first.toLowerCase() === second.toLowerCase()) {
    return 2;
  } else {
    return 0;
  }
}

// Stores the content of the html page to the variable html
const html = fs.readFileSync("./HTMLTestFile.htm", {
  encoding: "utf8",
  flag: "r",
});

const soup = new JSSoup(html);
let paragraph = soup.find("p");

// To keep count of the paragraph number
let no_of_paragraph = 0;

let data_obj_arr = [];

let index = 0;

while (paragraph) {
  // splits the text in the paragraphs with respect to "," space and newLine
  // filters the unwanted words like numbers, special characters and empty spaces
  let para_content = paragraph.text.split(/[,\s\n]/).filter(function (mov) {
    const pattern = /^[a-zA-Z]+$/;
    let result = pattern.test(mov);
    return result;
  });
  for (let i = 0; i < para_content.length; i++) {
    /*
      To create an array of objects(data_obj_arr) in the format [{word: 'theWord', position:'p2 : 23'}]
      word - contains the word
      position - contains the paragraph it belongs to and the position of the starting character
     */

    // pushes the object to the array(data_obj_arr)
    data_obj_arr.push({
      word: para_content[i],
      position: `p${no_of_paragraph} : ${paragraph.text.indexOf(
        `${para_content[i]}`
      )}`,
      index: index,
    });
    index++;
  }

  paragraph = paragraph.nextSibling;
  no_of_paragraph++;
}

// to keep track of the unsorted data
let org_data = [];
for (var item of data_obj_arr) {
  org_data.push(item);
}

// sorts the data inside of data_obj_arr with respect to word
// time taken to sort = 0.015 or 0.016 seconds
data_obj_arr.sort((a, b) => {
  return a.word.localeCompare(b.word);
});

/* This array contains the object in the format 
[
    {
      "color": {
        "p1": [3, 40]
      },
      "colour": {
        "p2": [8, 72]
      }
    },
    {
      "forty": {
        "p1": [3, 30]
      },
      "fourty": {
        "p2": [8, 50]
      }
    }
  ]
*/
let inconsistant_spelling_obj_arr = [];

/*
  Take the first word in the data_obj_arr and store it in the variable real_word
  
  initialize i = 1 
  Take the word in data_obj_arr and compare it with the real_word using the caseCheck function
  If the caseCheck function returns 1, then there is no inconsistant captialization. store the position of the data_obj_arr[i]
  If the caseCheck function returns 2, then there is inconsistant captialization. Store that word in inconsistant_wordand position in inconsistant_index
  If the caseCheck function returns 0, then it is a different word. pushed the stored values to inconsistant_spelling_obj_arr 
    change the real_word =  data_obj_arr[i].word, and reset all the values.
*/
let i = 1;
let real_word = data_obj_arr[0].word;
let actual_accurance = [];
let inconsistant_word;
let inconsistant_accurance = [];
let inconsistant_index = [];
let flag = 0;

while (data_obj_arr[i]) {
  if (caseCheck(data_obj_arr[i].word, real_word) == 1) {
    actual_accurance.push(data_obj_arr[i].position);
    // continue;
  } else if (caseCheck(data_obj_arr[i].word, real_word) == 2) {
    flag = 1;
    inconsistant_word = data_obj_arr[i].word;
    inconsistant_accurance.push(data_obj_arr[i].position);
    inconsistant_index.push(data_obj_arr[i].index);
    // continue;
  } else if (caseCheck(data_obj_arr[i].word, real_word) == 0) {
    if (flag) {
      let temp_obj = {
        real_word1: real_word,
        actual_accurance1: actual_accurance,
        inconsistant_word1: inconsistant_word,
        inconsistant_accurance1: inconsistant_accurance,
        inconsistant_index: inconsistant_index,
      };
      inconsistant_spelling_obj_arr.push(temp_obj);
      flag = 0;
      actual_accurance = [];
      inconsistant_word = "";
      inconsistant_accurance = [];
      inconsistant_index = [];
    }
    real_word = data_obj_arr[i].word;
  }
  i++;
}

/* This obj contains the contains the output with the required format
  output format:
  {
  "inconsistent-spelling": [
    {
      "color": {
        "p1": [3, 40]
      },
      "colour": {
        "p2": [8, 72]
      }
    },
    {
      "forty": {
        "p1": [3, 30]
      },
      "fourty": {
        "p2": [8, 50]
      }
    }
  ]
}
*/
let final_obj = {
  "Inconsistant-Spelling": inconsistant_spelling_obj_arr,
};
const myJSON = JSON.stringify(final_obj);
fs.writeFile("./solution1.txt", myJSON, (err) => {
  if (err) {
    console.error(err);
  }
});
const stop = Date.now();

console.log(`Time Taken to execute = ${(stop - start) / 1000} seconds`);
