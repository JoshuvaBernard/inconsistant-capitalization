const start = Date.now();

function hashStringToInt(s, tableSize) {
  let hash = 17;

  for (let i = 0; i < s.length; i++) {
    hash = (13 * hash * s.charCodeAt(i)) % tableSize;
  }

  return hash;
}

class HashTable {
  table = new Array(3333);
  numItems = 0;

  resize = () => {
    const newTable = new Array(this.table.length * 2);
    this.table.forEach((item) => {
      if (item) {
        item.forEach(([key, value]) => {
          const idx = hashStringToInt(key, newTable.length);
          if (newTable[idx]) {
            newTable[idx].push([key, value]);
          } else {
            newTable[idx] = [[key, value]];
          }
        });
      }
    });
    this.table = newTable;
  };

  setItem = (key, value) => {
    this.numItems++;
    const loadFactor = this.numItems / this.table.length;
    if (loadFactor > 0.8) {
      // resize
      this.resize();
    }

    const idx = hashStringToInt(key, this.table.length);
    if (this.table[idx]) {
      this.table[idx].push([key, value]);
    } else {
      this.table[idx] = [[key, value]];
    }
  };

  getItem = (key) => {
    const idx = hashStringToInt(key, this.table.length);

    if (!this.table[idx]) {
      return null;
    }

    // O(n)
    return this.table[idx].find((x) => x[0] === key)[1];
  };
}

const data = new HashTable();

var JSSoup = require("jssoup").default;

const fs = require("fs");

const html = fs.readFileSync("./HTMLTestFile.htm", {
  encoding: "utf8",
  flag: "r",
});

var soup = new JSSoup(html);
var paragraph = soup.find("p");

let no_of_paragraph = 0;

while (paragraph) {
  //   console.log("\n\n\npara no - " + no_of_paragraph);
  //   console.log(paragraph.text);
  let para_content = paragraph.text.split(/[,\s\n]/).filter(function (mov) {
    const pattern = /^[a-zA-Z]+$/;
    let result = pattern.test(mov);
    return result;
  });

  //   console.log(para_content);
  for (let i = 0; i < para_content.length; i++) {
    // filter out unwanted
    // if()
    let j;
    data.setItem(`p${no_of_paragraph} : [${i}]`, para_content[i]);
  }

  paragraph = paragraph.nextSibling;
  no_of_paragraph++;
}

// console.log(data);

// const sorted_data = data.sort((a, b) => {
//   console.log("kk");
//   return a.value.localeCompare(b.value);
// });
// // console.log(sorted_data);

function sortObjectByKeys(o) {
  return Object.keys(o)
    .sort()
    .reduce((r, k) => ((r[k] = o[k]), r), {});
}
// var unsorted = {"c":"crane","b":"boy","a":"ant"};
// var sorted=sortObjectByKeys(unsorted);
var unsorted = { c: "crane", b: "boy", a: "ant" };
var sorted = sortObjectByKeys(data);

console.log(sorted);

const myJSON = JSON.stringify(sorted);
fs.writeFile("./test11.txt", myJSON, (err) => {
  if (err) {
    console.error(err);
  }
});

const stop = Date.now();
console.log(`Time Taken to execute = ${(stop - start) / 1000} seconds`);
