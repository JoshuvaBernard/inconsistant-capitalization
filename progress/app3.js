const start = Date.now();
const { convert } = require("html-to-text");
const fs = require("fs");

const html = fs.readFileSync("./HTMLTestFile.htm", {
  encoding: "utf8",
  flag: "r",
});

const text = convert(html, {
  wordwrap: 130,
});

let text_arr = text.split(/[,\s\n]/).filter(function (mov) {
  const pattern = /^[a-zA-Z]+$/;
  let result = pattern.test(mov);
  return result;
});



const obj = [];
for (let i = 0; i < text_arr.length; i++) {
  let temp = {
    word: text_arr[i],
    index: i,
  };
  obj.push(temp);
}

const text_sorted = obj.sort((a, b) => {
  return a.word.localeCompare(b.word);
  //   return a.word.localeCompare(b.word, undefined, { sensitivity: "base" });
});

console.log(text_sorted);

function caseCheck(current, previous) {
  if (current === previous) {
    return 1;
  } else if (current.toLowerCase() === previous.toLowerCase()) {
    return 2;
  } else {
    return 0;
  }
}

let i = 1;
let temp = text_sorted[0].word;

let inConsisitantWords = [];
let Actuall_accurance = [];
let inconsistant_accurance = [];
let inConsisitant_Cap;
let flag = 0;
while (text_sorted[i]) {
  //   console.log(text_sorted[i]);
  if (caseCheck(temp, text_sorted[i].word) == 1) {
    // console.log("w1");
    Actuall_accurance.push(text_sorted[i].index);
  } else if (caseCheck(temp, text_sorted[i].word) == 2) {
    flag = 1;
    inConsisitant_Cap = text_sorted[i].word;
    inconsistant_accurance.push(text_sorted[i].index);
    // console.log("w2");
  } else {
    // console.log("w3");
    if (flag) {
      let obj = {
        realWord: temp,
        Actuall_accurance: Actuall_accurance,
        inConsisitant_Cap: inConsisitant_Cap,
        inconsistant_accurance: inconsistant_accurance,
      };
      inConsisitantWords.push(obj);
      Actuall_accurance = [];
      inConsisitant_Cap = "";
      inconsistant_accurance = [];
      flag = 0;
    }
    temp = text_sorted[i].word;
  }
  i++;
}

console.log(inConsisitantWords);
const myJSON = JSON.stringify(inConsisitantWords);
fs.writeFile("./test3.txt", myJSON, (err) => {
  if (err) {
    console.error(err);
  }
});

const stop = Date.now();
console.log(`Time Taken to execute = ${(stop - start) / 1000} seconds`);

