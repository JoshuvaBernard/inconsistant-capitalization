// sorted

const start = Date.now();
const data_map = new Map();
var JSSoup = require("jssoup").default;
const fs = require("fs");

const html = fs.readFileSync("./HTMLTestFile.htm", {
  encoding: "utf8",
  flag: "r",
});

var soup = new JSSoup(html);
var paragraph = soup.find("p");

let no_of_paragraph = 0;

function caseCheck(current, previous) {
  if (current === previous) {
    return 1;
  } else if (current.toLowerCase() === previous.toLowerCase()) {
    return 2;
  } else {
    return 0;
  }
}

while (paragraph) {
  //   console.log("\n\n\npara no - " + no_of_paragraph);
  //   console.log(paragraph.text);
  // filter out unwanted

  let para_content = paragraph.text.split(/[,\s\n]/).filter(function (mov) {
    const pattern = /^[a-zA-Z]+$/;
    let result = pattern.test(mov);
    return result;
  });

  //   console.log(para_content);

  // try adding the filter within this for loop
  for (let i = 0; i < para_content.length; i++) {
    let j;
    data_map.set(`p${no_of_paragraph} : ${i}`, para_content[i]);
  }

  paragraph = paragraph.nextSibling;
  no_of_paragraph++;
}

const sorted_data_map = new Map(
  [...data_map].sort((a, b) => {
    return a[1].localeCompare(b[1]);
    //   return a.word.localeCompare(b.word, undefined, { sensitivity: "base" });
  })
);

// console.log(sorted_data_map);

const sorted_data_map_WithIndex = new Map();

let i = 0;
sorted_data_map.forEach(function (value, key) {
  sorted_data_map_WithIndex.set(i, { word: value, position: key });
  i++;
});
// console.log(sorted_data_map_WithIndex);
// console.log(sorted_data_map_WithIndex.get(0).word);

let real_word;
let actual_accurance = [];
let inconsistant_word;
let inconsistant_accurance = [];
let flag = 0;

real_word = sorted_data_map_WithIndex.get(0).word;

let final_obj = [];


sorted_data_map_WithIndex.forEach(function (value, key) {
  if (caseCheck(value.word, real_word) == 1) {
    actual_accurance.push(value.position);
    // console.log(value);
  } else if (caseCheck(value.word, real_word) == 2) {
    flag = 1;
    inconsistant_word = value.word;
    inconsistant_accurance.push(value.position);
  } else {
    if (flag) {
      let temp_obj = {
        real_word1: real_word,
        actual_accurance1: actual_accurance,
        inconsistant_word1: inconsistant_word,
        inconsistant_accurance1: inconsistant_accurance,
      };
      final_obj.push(temp_obj);
      flag = 0;
      actual_accurance = [];
      inconsistant_word = "";
      inconsistant_accurance = [];
    }
    real_word = value.word
  }
});
console.log(final_obj);
// sorted_data_map_WithIndex.forEach(function (value, key) {
//   if(sorted_data_map_WithIndex.get(key - 1)){
//     if(caseCheck(value.word, sorted_data_map_WithIndex.get(key - 1).word) == 1){
//       actual_accurance.push(key);
//       // console.log(value);
//     } else if(caseCheck(value.word, sorted_data_map_WithIndex.get(key - 1).word) == 2){
//       flag = 1;
//       inconsistant_word = sorted_data_map_WithIndex.get(key - 1).word;
//       inconsistant_accurance = sorted_data_map_WithIndex.get(key - 1).word
//     }
//   }
//   // console.log(value);
//   // console.log(key);

// });

// const obj_data = Object.fromEntries(sorted_data_map);

// const myJSON = JSON.stringify(final_obj);
// // console.log(myJSON);
// fs.writeFile("./test6.txt", myJSON, (err) => {
//   if (err) {
//     console.error(err);
//   }
// });

const stop = Date.now();
console.log(`Time Taken to execute = ${(stop - start) / 1000} seconds`);
