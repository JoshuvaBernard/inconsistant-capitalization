// const arr = ['Z', 'A', 'a', 'C'];

// const sorted = arr.sort((a, b) => {
//   return a.localeCompare(b, undefined, {sensitivity: 'base'});
// });

// console.log(sorted); // 👉️ ['a', 'C', 'f', 'Z']

// const strings = ['č','é','A','b','Đ'];

// const defaultSort = Array.from(strings).sort();

// const simpleSort = Array.from(strings).sort((a, b) => a - b);

// const localeSort = Array.from(strings).sort((a, b) => {
//   return a.localeCompare(b, 'en', { sensitivity: 'base' });
// });

// console.log(defaultSort);
// console.log(simpleSort);
// console.log(localeSort);

const students = [
  { firstName: "John", lastName: "Doe", graduationYear: 2022 },
  { firstName: "Stephen", lastName: "Matt", graduationYear: 2023 },
  { firstName: "Abigail", lastName: "Susu", graduationYear: 2022 },
  { firstName: "Zara", lastName: "Kate", graduationYear: 2024 },
  { firstName: "Daniel", lastName: "Vic", graduationYear: 2023 },
];
let sortedStudents = students.sort((a, b) => {
  if (a.firstName < b.firstName) {
    return -1;
  }
  if (a.firstName > b.firstName) {
    return 1;
  }
  return 0;
});

console.log(sortedStudents);
