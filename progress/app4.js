const start = Date.now();
const { convert } = require("html-to-text");
const fs = require("fs");

const html = fs.readFileSync("./HTMLTestFile.htm", {
  encoding: "utf8",
  flag: "r",
});

const text = convert(html, {
  wordwrap: 130,
});

let text_arr = text.split(/[,\s\n]/).filter(function (mov) {
  const pattern = /^[a-zA-Z]+$/;
  let result = pattern.test(mov);
  return result;
});

let org_text_obj = [];
for (let i = 0; i < text_arr.length; i++) {
  let temp = {
    word: text_arr[i],
    index: i,
  };
  org_text_obj.push(temp);
}

console.log(org_text_obj[68]);

let org = [];
for (var pp of org_text_obj) {
  org.push(pp);
}
// const org = org.push(...org_text_obj);

const myJSON1 = JSON.stringify(org);
fs.writeFile("./test5.txt", myJSON1, (err) => {
  if (err) {
    console.error(err);
  }
});

const sorted_text_obj = org_text_obj.sort((a, b) => {
  return a.word.localeCompare(b.word);
  //   return a.word.localeCompare(b.word, undefined, { sensitivity: "base" });
});

function caseCheck(current, previous) {
  if (current === previous) {
    return 1;
  } else if (current.toLowerCase() === previous.toLowerCase()) {
    return 2;
  } else {
    return 0;
  }
}

let i = 0;
let real_word = sorted_text_obj[0].word;
let actual_accurance = [];
let inconsistant_word;
let inconsistant_accurance = [];
let flag = 0;

// let temp_obj = {
//   real_word1: "",
//   actual_accurance1: [],
//   inconsistant_word1: "",
//   inconsistant_accurance1: [],
// };

let final_obj = [];

while (sorted_text_obj[i]) {
  if (caseCheck(sorted_text_obj[i].word, real_word) == 1) {
    actual_accurance.push(sorted_text_obj[i].index);
    // continue;
  } else if (caseCheck(sorted_text_obj[i].word, real_word) == 2) {
    flag = 1;
    inconsistant_word = sorted_text_obj[i].word;
    inconsistant_accurance.push(sorted_text_obj[i].index);
    // continue;
  } else {
    if (flag) {
      let temp_obj = {
        real_word1: real_word,
        actual_accurance1: actual_accurance,
        inconsistant_word1: inconsistant_word,
        inconsistant_accurance1: inconsistant_accurance,
      };
      final_obj.push(temp_obj);
      flag = 0;
      actual_accurance = [];
      inconsistant_word = "";
      inconsistant_accurance = [];
    }
    real_word = sorted_text_obj[i].word;
  }
  i++;
}

const myJSON = JSON.stringify(final_obj);
fs.writeFile("./test4.txt", myJSON, (err) => {
  if (err) {
    console.error(err);
  }
});

const stop = Date.now();
console.log(`Time Taken to execute = ${(stop - start) / 1000} seconds`);

console.log(org[68]);
// histogram program, word frequency
