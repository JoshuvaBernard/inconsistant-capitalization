const { convert } = require("html-to-text");
// There is also an alias to `convert` called `htmlToText`.

const html = `

<!-- saved from url=(0134)https://resources-testing.kriyadocs.com/resources/bmj/ejhpharm/ejhpharm-2022-003293/resources/50366251-c64d-4710-b094-2c37abf9eef8.htm -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="Generator" content="Microsoft Word 14 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:10.0pt;
	margin-left:0in;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
h1
	{mso-style-link:"Heading 1 Char";
	margin-right:0in;
	margin-left:0in;
	font-size:24.0pt;
	font-family:"Times New Roman","serif";}
h2
	{mso-style-link:"Heading 2 Char";
	margin-right:0in;
	margin-left:0in;
	font-size:18.0pt;
	font-family:"Times New Roman","serif";}
h3
	{mso-style-link:"Heading 3 Char";
	margin-right:0in;
	margin-left:0in;
	font-size:13.5pt;
	font-family:"Times New Roman","serif";}
span.Heading1Char
	{mso-style-name:"Heading 1 Char";
	mso-style-link:"Heading 1";
	font-family:"Times New Roman","serif";
	font-weight:bold;}
span.Heading2Char
	{mso-style-name:"Heading 2 Char";
	mso-style-link:"Heading 2";
	font-family:"Times New Roman","serif";
	font-weight:bold;}
span.Heading3Char
	{mso-style-name:"Heading 3 Char";
	mso-style-link:"Heading 3";
	font-family:"Times New Roman","serif";
	font-weight:bold;}
span.MTConvertedEquation
	{mso-style-name:MTConvertedEquation;
	font-family:"Arial","sans-serif";
	color:black;
	font-weight:bold;}
span.EPSsmallCaps
	{mso-style-name:EPSsmallCaps;
	font-family:"Calibri","sans-serif";
	color:black;
	font-weight:bold;}
.MsoChpDefault
	{font-family:"Calibri","sans-serif";}
.MsoPapDefault
	{margin-bottom:10.0pt;
	line-height:115%;}
@page WordSection1
	{size:595.3pt 841.9pt;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
-->
</style>

</head>

<body lang="EN-US">

<div class="WordSection1">

<p class="MsoNormal" style="margin-top:24.0pt;margin-right:0in;margin-bottom:
0in;margin-left:.25in;margin-bottom:.0001pt;text-indent:-.25in;line-height:
normal"><b><span lang="EN-IN" style="font-size:16.0pt;color:black">The effect of
product line endpoint prices on vertical extensions</span></b></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Je<span style="background:white">á</span>n Baptist<span style="background:white">é</span></span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Correspondence
to: </span><span lang="EN-IN"><a href="mailto:jeanbap@kdx.com"><span style="font-size:12.0pt;color:blue">jeanbap@kdx.com</span></a></span></p>

<p class="MsoNormal" style="margin-top:12.0pt;margin-right:0in;margin-bottom:
3.0pt;margin-left:0in;text-align:justify;line-height:normal"><b><span lang="EN-IN" style="font-size:16.0pt;color:black">Abstract</span></b></p>

<p class="MsoNormal" style="line-height:normal"><b><span lang="EN-IN" style="font-size:12.0pt;color:black">Purpose<br>
</span></b><span lang="EN-IN" style="font-size:12.0pt;color:black">Literature</span><span lang="EN-IN" style="font-size:9.0pt;color:black;background:white">, </span><span lang="EN-IN" style="font-size:12.0pt;color:black">in Brand Extensions has relied
greatly on categorization theory and on prototypical models of categorization
to explain affect transfer from a parent brand to its extensions. Drawing on
range theory and on exemplar models of categorization this research shows the
effects of parent brand endpoint prices on consumer judgments of vertical line
extensions.</span></p>

<p class="MsoNormal" style="line-height:normal"><b><span lang="EN-IN" style="font-size:12.0pt;color:black">Design/methodology/approach<br>
</span></b><span lang="EN-IN" style="font-size:12.0pt;color:black">Three
experiments were conducted. Experiment 1 tests the hypothesis consumers rely on
the parent brand price range when making judgments of an upscale extensions.
Experiment 2 tests the hypothesis that the effect of price range on extension
evaluation is mediated by perceived risks for upscale extensions but not
downscale extensions. The final experiment shows a boundary condition to the
product line range effect on upscale extensions.</span></p>

<p class="MsoNormal" style="line-height:normal"><b><span lang="EN-IN" style="font-size:12.0pt;color:black">Findings<br>
</span></b><span lang="EN-IN" style="font-size:12.0pt;color:black">This research
shows that up-scale extensions are judged more favorably in the context of a
wide versus a narrow product line even when the highest endpoint in both
product lines are equally close to the extension and that this effect is
mediated by perceived consistency and perceived risk. The range effect
disappears, however, when consumers have a broad focus in which attention
shifts to category endpoint prices, making parent brand prices less diagnostic
of upscale extension judgments.</span></p>

<p class="MsoNormal" style="line-height:normal"><b><span lang="EN-IN" style="font-size:12.0pt;color:black">Practical implications<br>
</span></b><span lang="EN-IN" style="font-size:12.0pt;color:black">Managers may
display a wider range of products and/or reduce prices of low-end models to
expand product line price width. In consequence, low-end products become more
competitive in terms of price and at the same time improve favourability
ratings of the new upscale product.</span></p>

<p class="MsoNormal" style="line-height:normal"><b><span lang="EN-IN" style="font-size:12.0pt;color:black">Originality/value<br>
</span></b><span lang="EN-IN" style="font-size:12.0pt;color:black">Vertical line
extensions and product line pricing are important topics to both academics and
practitioners. Nonetheless, this is the first research to demonstrate how
product line price width can influence consumer perceptions of vertical line
extension.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><b><span lang="EN-IN" style="font-size:12.0pt;color:black">Introduction</span></b></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Vertical
differentiation is a common growth strategy brand managers use to attract more
customers, satisfying their desire for variety (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.30j0zll"><span style="font-size:12.0pt;color:black">Lancaster, 1990</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1fob9te"><span style="font-size:12.0pt;color:black">Quelch and Kenny, 1994</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) and willingness to pay for
quality (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3znysh7"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Moorthy, 1984</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). By adding downscale
extensions (e.g., Giorgio Armani, Armani Collezioni, Armani Jeans), brands
attract customers who may not be able to afford current brand offerings,
increasing usage rate, and creating new sources of brand equity. Conversely,
stretching the brand upward (e.g., Johnnie Walker’s Red Label, Gold Label
Reserve, Blue Label) enables the brand to access consumers who are looking for
more features, greater prestige, or higher quality products (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2et92p0"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Aaker, 1997</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.tyjcwt"><span style="font-size:12.0pt;color:black">Kirmani<i> et al.</i>, 1999</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). Despite the many advantages
of a vertical differentiation strategy, brand competition is intense and
success can be hard to come by. A study by Nielsen (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3dy6vkm"><span style="font-size:12.0pt;color:black">2015</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) shows that 45% of new products
introduced in Europe between 2011 and 2013 were removed from the market in the
first six months and only 24% made it to reach a full year.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">To
increase the likelihood of new product acceptance, scholars have investigated
many dimensions that influence an extension’s success (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1t3h5sf"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Nijssen, 1999</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.4d34og8"><span style="font-size:12.0pt;color:black">Reddy<i> et al.</i>, 1994</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2s8eyo1"><span style="font-size:12.0pt;color:black">Volckner and Sattler, 2006</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). In particular, research on
vertical extensions has investigated how the number of products in the line (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.17dp8vu"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Dacin and Smith, 1994</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">), extension direction, brand
concept (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3rdcrjn"><span style="font-size:12.0pt;color:black">Kim<i> et al.</i>, 2001</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">), brand ownership (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.26in1rg"><span style="font-size:12.0pt;color:black">Fu<i> et al.</i>, 2009</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.tyjcwt"><span style="font-size:12.0pt;color:black">Kirmani<i> et al.</i>, 1999</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">), perceived risk (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.lnxbz9"><span style="font-size:12.0pt;color:black">Lei<i> et al.</i>, 2008</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">), and extension’s price point
(</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.35nkun2"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Dall'Olmo Riley<i> et
al.</i>, 2013</span></a></span><span lang="EN-IN" style="font-size:12.0pt;
color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1ksv4uv"><span style="font-size:12.0pt;color:black">Pontes<i> et al.</i>, 2017</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) influence extension
evaluations. Despite the growing interest in the factors affecting vertical
extension’s success, there have been no studies examining the effect of parent
brand price structure on consumer judgments.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Building
on the idea that consumers store, retrieve, and use a rich array of price
information in the process of generating price judgments (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.44sinio"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Janiszewski and
Lichtenstein, 1999</span></a></span><span lang="EN-IN" style="font-size:12.0pt;
color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2jxsxqh"><span style="font-size:12.0pt;color:black">Niedrich<i> et al.</i>, 2001</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.z337ya"><span style="font-size:12.0pt;color:black">Niedrich<i> et al.</i>, 2009</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">), this research shows that an
upscale extension will be judged more favorably in the context of a wide versus
a narrow product line even when the highest endpoint in both product lines are
equally close to the extension. <i>Ceteris paribus</i>, judgments of upscale
extensions improve with decreases in price point of a brand’s lowest-tier
model. This notion is consistent with the range principle (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3j2qqm3"><span style="font-size:12.0pt;color:black">Volkmann, 1951</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) which states that one’s
ability to discriminate between two numbers decreases as the stimulus range
increases (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1y810tw"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Gravetter and Lockhead,
1973</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.4i7ojhp"><span style="font-size:12.0pt;color:black">Poulton, 1968</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2xcytpi"><span style="font-size:12.0pt;color:black">Teghtsoonian, 1971</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1ci93xb"><span style="font-size:12.0pt;color:black">Wedell, 2008</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). Therefore, a vertical
extension should be perceived as more consistent with the current parent brand
and thus rated more positively when judged in the context of a wide than narrow
product line price range.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Furthermore,
this paper demonstrates that the product line range effect is observed for
upscale but not for downscale extensions because the effect of perceived
consistency on consumer judgments decreases with lower levels of risk (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3whwml4"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">DelVecchio and Smith,
2005</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1ksv4uv"><span style="font-size:12.0pt;color:black">Pontes<i> et al.</i>, 2017</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). In turn, the likelihood that
consumers will use a brand’s product line range as cue to diminish perceptions
of risk and make judgments of downscale extensions is reduced. Finally, this
paper shows that the range effect on upscale extensions holds when consumer’s
attention focus is specific to parent brand prices but the effect dissipates
when the frame of reference shifts to prices in the product category. Empirical
evidence for the proposed hypothesis are provided in a series of four
experiments.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><b><span lang="EN-IN" style="font-size:12.0pt;color:black"><br>
Theoretical Background</span></b></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Researchers in
the area of behavioral pricing have extensively discussed how consumers form
and use price referents as comparison standards in price judgments has long
intrigued pricing researchers (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2bn6wsx"><span style="font-size:12.0pt;color:black">Cunha and Shulman, 2011</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.qsh70q"><span style="font-size:12.0pt;color:black">Kalyanaram and Winer, 1995</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3as4poj"><span style="font-size:12.0pt;color:black">Mazumdar<i> et al.</i>, 2005</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1pxezwc"><span style="font-size:12.0pt;color:black">Monroe, 1973</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). One account for how
consumers form reference prices is based on adaptation-level theory (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.49x2ik5"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Helson, 1947</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) which asserts that past
experiences and present context define an adaptation-level, or reference point,
relative to which new stimuli are perceived and compared. In a pricing context,
the adaptation-level is described as a single-point summary measure of the
price distribution that is typically represented by an arithmetic mean of
previously encountered market prices (</span><span lang="EN-IN" style="font-size:
12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3as4poj"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Mazumdar<i> et al.</i>,
2005</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">)
but that does not necessarily correspond to any actual price (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2p2csry"><span style="font-size:12.0pt;color:black">Cheng and Monroe, 2013</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.147n2zr"><span style="font-size:12.0pt;color:black">Monroe, 1990</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">).</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">However,
many researchers provide strong evidence against central tendency models to
account for the effects of reference price distribution range, modality, and
skewing on consumer price perceptions (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.44sinio"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Janiszewski and
Lichtenstein, 1999</span></a></span><span lang="EN-IN" style="font-size:12.0pt;
color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2jxsxqh"><span style="font-size:12.0pt;color:black">Niedrich<i> et al.</i>, 2001</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.z337ya"><span style="font-size:12.0pt;color:black">Niedrich<i> et al.</i>, 2009</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). Over several experiments,
Niedrich et al. (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2jxsxqh"><span style="font-size:12.0pt;color:black">2001</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.z337ya"><span style="font-size:12.0pt;color:black">2009</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) show that the highest and lowest prices
in a distribution are the most salient and thus more easily retrieved from
memory than other price cues. Consequently, price judgment depends on the
location of a new target price relative to the end-points in a set of prices, a
notion that is consistent with predictions of the range theory (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3j2qqm3"><span style="font-size:12.0pt;color:black">Volkmann, 1951</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">).</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><b><i><span lang="EN-IN" style="font-size:12.0pt;color:black">The Range
Principle</span></i></b></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Range theory is
based on a range principle of judgment in which a target stimulus is judged
relative to its location on a subjective scale comprised by the most and least
extreme values evoked from memory and brought to mind at the time of judgment (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3o7alnk"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Parducci, 1965</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3j2qqm3"><span style="font-size:12.0pt;color:black">Volkmann, 1951</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). Empirical evidence showing
that consumer judgments of target prices within a distribution set is subject
to endpoint anchors (lowest and highest prices) is vast in the consumer
research literature (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2bn6wsx"><span style="font-size:12.0pt;color:black">Cunha and Shulman, 2011</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.44sinio"><span style="font-size:12.0pt;color:black">Janiszewski and Lichtenstein, 1999</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2jxsxqh"><span style="font-size:12.0pt;color:black">Niedrich<i> et al.</i>, 2001</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.z337ya"><span style="font-size:12.0pt;color:black">Niedrich<i> et al.</i>, 2009</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.23ckvvd"><span style="font-size:12.0pt;color:black">Yeung and Soman, 2005</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). For instance, Janiszewski
and Lichtenstein (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.44sinio"><span style="font-size:12.0pt;color:black">1999</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) demonstrate that a new price of $1.25
perceived to be cheaper, thus more attractive, in the context of a narrow price
range ($1.00 to $2.00) compared to the context of a wide price range ($0.50 to
$2.50) because the new price is ranked lower and, therefore, perceived to be
closer to the lower endpoint in the narrow versus the wide price distribution.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Range
effects have also been shown to influence magnitude estimations such that the
perceived difference between two target stimuli is smaller when they are judged
in the context of a wide versus a narrow range (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.ihv636"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Mellers and Cooke, 1994</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.23ckvvd"><span style="font-size:12.0pt;color:black">Yeung and Soman, 2005</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). For example, a consumer may
report perceiving a smaller difference between fuel efficiencies of 32 miles
per gallon (mpg) and 28 mpg when judgment is made in the context of cars
getting 18 mpg to 42 mpg (wide range) than in the context of cars getting 24
mpg to 36 mpg (narrow range). While most research examining range effects focus
on targets <i>within</i> a distribution set, Sherif <i>et al</i>. (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.32hioqz"><span style="font-size:12.0pt;color:black">1958</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) examined magnitude judgments when a
target stimulus was introduced <i>outside</i> the original distribution set. In
their study, participants were asked to judge differences of an original
outside series consisting of weights of 55 gm, 75 gm, 93 gm, 109 gm, 125 gm,
and 141 gm after the presentation of an anchor stimulus with values ranging
from 141 gm to 347 gm. Results reported show that when an anchor was introduced
at the end (141 gm) or slightly removed from the end (168 gm) of the
distribution set, one’s ability to discriminate weights between 55 gm and 141
gm increased. In contrast, when the target stimulus was placed too far (347 gm)
from the original distribution set, discrimination performance decreased with a
constriction of the judgment scale.</span></p>

<p class="MsoNormal" align="center" style="text-align:center;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">----Insert Figure 1 here----</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Grounded
on findings of Sherif <i>et al. </i>(</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.32hioqz"><span style="font-size:12.0pt;color:black">1958</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">), one can infer that a consumer would
report perceiving a smaller difference between an anchor weight of 218 gm and
the higher end of a range weighting 141 gm when judgment is made in a wide
range context consisting of weights between 55 gm and 141 gm than in a narrow
range context consisting of weights between 109 gm and 141 gm (refer to Figure
1). Despite that absolute differences between 141 gm and 218 gm is an equal 77
gm in both cases, one’s judgment scale is constricted when exposed to a wider
range of weights, changing relative magnitude perceptions such that the perceived
distance between target and standard is smaller in a wide versus a narrow
context. The wider the stimulus range, the shallower the slope of the
psychophysical function which means that one’s ability to discriminate between
two numbers decreases as the stimulus range increases (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1y810tw"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Gravetter and Lockhead,
1973</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.4i7ojhp"><span style="font-size:12.0pt;color:black">Poulton, 1968</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2xcytpi"><span style="font-size:12.0pt;color:black">Teghtsoonian, 1971</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1ci93xb"><span style="font-size:12.0pt;color:black">Wedell, 2008</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">).</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">In
a similar vein, product line prices have the special role of signaling
consumers the extent of which extensions are consistent with the parent brand
by influencing one’s ability to discriminate the new extension price from
current brand offering. The larger the perceived distance between the parent
brand and the extension in the price/quality spectrum, the lower the perceived
consistency between them (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.lnxbz9"><span style="font-size:12.0pt;color:black">Lei<i> et al.</i>, 2008</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). Consistent with predictions
of the range principle, an upscale extension will be perceived to be more
consistent with its parent brand and, therefore, judged more favorably in the
context of a wide versus a narrow product line even when the highest endpoint
in both product lines are equally close to the extension.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black"><br>
<br>
<br>
<br>
</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><b><i><span lang="EN-IN" style="font-size:12.0pt;color:black">Extension
Direction and the range effect</span></i></b></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">A common finding
in the brand extension literature is that extension’s success depends on its
perceived consistency or fit with the parent brand such that the higher the
perceived consistency, the more positive the extension evaluation (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2s8eyo1"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Volckner and Sattler,
2006</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">).
However, the extent consumers rely on perceived consistency is a function of
the amount of risk present at the time of judgment (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3whwml4"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">DelVecchio and Smith,
2005</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">).
In a vertical extension context, perceived consistency is particularly
important for upscale extensions because higher monetary sacrifices are
inherent to higher priced products (</span><span lang="EN-IN" style="font-size:
12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1hmsyys"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Shimp and Bearden, 1982</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). A lower consistency between
an upscale extension and its parent brand signal consumers that the brand may
be stretching beyond its expertise (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.41mghml"><span style="font-size:12.0pt;color:black">Keller and Aaker, 1992</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2grqrue"><span style="font-size:12.0pt;color:black">Smith and Andrews, 1995</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">), making consumers more
skeptical about the brand's credibility in delivering the appropriate benefits
of more upscale markets (</span><span lang="EN-IN" style="font-size:12.0pt;
font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2et92p0"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Aaker, 1997</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">).</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">On
the other hand, downscale extensions should yield lower perceived risks than
upscale extensions because consumers tend to be more confident that the brand
will meet their expectations at lower price tiers when the parent brand has
previously serviced a higher priced segment. Indeed, research on vertical
extensions has shown that parent brand expertise acts as a risk reduction
mechanism on downscale extensions (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1ksv4uv"><span style="font-size:12.0pt;color:black">Pontes<i> et al.</i>, 2017</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). Since consumers’ reliance on
perceived consistency decreases with lower levels of risk (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3whwml4"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">DelVecchio and Smith,
2005</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">),
it is expected that consumers do not use the product line range as cue to
diminish perceptions of risk and make judgments of downscale extensions.
Therefore, the following hypotheses are proposed:</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">H</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">1</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black">: For upscale extensions, a narrow product
line will lead to (a) lower perceived consistency, (b) higher perceived risks,
and (c) less positive extension evaluations than a wide product line.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">H</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">2</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black">: For downscale extensions, (a)
perceptions of risk are not influenced by product line width and as consequence
(b) evaluations of a narrow product line will be equal to a wide product line.</span></p>

<p class="MsoNormal" style="margin-top:12.0pt;margin-right:0in;margin-bottom:
3.0pt;margin-left:0in;text-align:justify;text-indent:-.35in;line-height:normal"><b><span lang="EN-IN" style="font-size:13.0pt;color:black">Attention Focus And The Range
Effect</span></b></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Prior research on
joint versus separate evaluation modes has shown that consumer preferences
formed in isolation of competition tend to differ from those judgments formed
when competitors are present (</span><span lang="EN-IN" style="font-size:12.0pt;
font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.vx1227"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Hsee and Leclerc, 1998</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3fwokq0"><span style="font-size:12.0pt;color:black">Hsee<i> et al.</i>, 1999</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1v1yuxt"><span style="font-size:12.0pt;color:black">Leclerc<i> et al.</i>, 2005</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). In the absence of
competition, consumers tend to have a narrow focus of attention and use the
immediate category (e.g., parent brand) as their frame of reference, leading to
an overemphasis on perceived consistency (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.4f1mdlm"><span style="font-size:12.0pt;color:black">Milberg<i> et al.</i>, 2010</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) and to positive bias due to
selective processing (</span><span lang="EN-IN" style="font-size:12.0pt;
font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2u6wntf"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Posavac<i> et al.</i>,
2005</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.19c6y18"><span style="font-size:12.0pt;color:black">Posavac<i> et al.</i>, 2004</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). Therefore, consumers in a
separate evaluation mode tend to rely on parent brand prices to form the
standard of comparison when make judgments of vertical extensions which in turn
results in the proposed product line range effect.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">In
the presence of competition, however, consumers expand their focus of attention
(</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1v1yuxt"><span style="font-size:12.0pt;color:black">Leclerc<i> et al.</i>, 2005</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) when forming a standard of
comparison because additional cues (e.g., competitors’ prices) decrease the
predictive validity of a single cue in formulating evaluations (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3tbugp1"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Purohit and Srivastava,
2001</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">).
When consumers have a broad focus, the end-points that anchor the frame of
reference shifts from the parent brand price range to the category’s highest
and lowest prices. Because the range principle states that the judgment of a
target within a set of stimuli is dependent on its relationship to the minimum
and maximum contextual values, the product line range effect should disappear
for upscale extensions. Therefore, the following hypothesis is proposed:</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">H</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">3</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black">: for upscale extensions, people will be
less likely to (a) rely on the perceived consistency and (b) exhibit a product
line range effect when the attention focus is broad than when it is narrow.</span></p>

<p class="MsoNormal" style="margin-top:12.0pt;margin-right:0in;margin-bottom:
3.0pt;margin-left:0in;text-align:justify;line-height:normal"><b><span lang="EN-IN" style="font-size:16.0pt;color:black">Overview Of Experiments</span></b></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">The empirical
work presented in this article consists of four studies that were designed to
test the proposed hypotheses. Experiment 1 tests the product line price range
hypothesis by manipulating the parent brand price range such that for upscale
scenarios, the highest price point is the same for both narrow and wide
conditions resulting in a higher average price for the narrow versus the wide
product line. Conversely, for the downscale scenario, the lowest end price is
the same for both narrow and wide conditions such that a lower average price is
found in the narrow versus the wide product line. Experiment 2 tests the
hypothesis that the effect of price range on extension evaluation is mediated
by perceived risks for upscale extensions but not downscale extensions.
Finally, experiment 3 demonstrates how consumer focus acts as a boundary
condition to the product line range effect on upscale extensions.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><i><span lang="EN-IN" style="font-size:12.0pt;color:black">Sample
Characteristics</span></i></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">In experiment 1,
participants were University students (59% males) between 18 and 30 years of
age (median age = 20), with a modal income category between $10,000 and $20,000
per annum, who were recruited to participate in this study in exchange for
course credit. In experiments 2 and 3, participants were recruited from
Amazon’s Mechanical Turk (MTurk), a large crowdsourcing marketplace. Similar to
other studies using MTurk (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.28h4qwu"><span style="font-size:12.0pt;color:black">Arndt<i> et al.</i>, 2016</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.nmf14n"><span style="font-size:12.0pt;color:black">Biswas<i> et al.</i>, 2013</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.37m2jsg"><span style="font-size:12.0pt;color:black">Palmeira<i> et al.</i>, 2016</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">), respondents received a
nominal fee to complete the survey (between $0.60 and $0.75). Participants were
54% female, between 18 and 73 years of age (median age = 32), and with a modal
category of individual annual income between $20,000 and $35,000. Indeed, prior
research has shown that MTurk members tend to be younger, more educated, have
lower income, and be more ideologically liberal as compared to the average U.S.
population (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1mrcu09"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Berinsky<i> et al.</i>,
2012 </span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.46r0co2"><span style="font-size:12.0pt;color:black">Feitosa<i> et al.</i>, 2015</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2lwamvv"><span style="font-size:12.0pt;color:black">Smith<i> et al.</i>, 2016</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). Nonetheless, research shows
that MTurk workers are more attentive to survey instructions (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.111kx3o"><span style="font-size:12.0pt;color:black">Hauser and Schwarz, 2016</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) and provide reliable
experimental data in judgment and decision-making (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3l18frh"><span style="font-size:12.0pt;color:black">Goodman<i> et al.</i>, 2013</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.206ipza"><span style="font-size:12.0pt;color:black">Paolacci<i> et al.</i>, 2010</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.4k668n3"><span style="font-size:12.0pt;color:black">Thomas<i> et al.</i>, 2015</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). Therefore, suitable for the
current research.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><b><span lang="EN-IN" style="font-size:12.0pt;color:black">Experiment 1</span></b></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">This experiment
was designed with three goals in mind. The goal of the first experiment was to
test empirically the hypothesis that consumers rely on the parent brand price
width to make judgments of upscale extensions but not downscale extensions. A
second goal was to provide empirical evidence against an alternative account
based on a reference price formed by averaging process of parent brand prices.
Lastly, because previous studies have suggested that brand concept may be an
important variable to consider in brand extensions (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2zbgiuw"><span style="font-size:12.0pt;color:black">Park<i> et al.</i>, 1991</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) and, in particular, vertical
extension research (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1egqt2p"><span style="font-size:12.0pt;color:black">Heath<i> et al.</i>, 2011</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3rdcrjn"><span style="font-size:12.0pt;color:black">Kim<i> et al.</i>, 2001</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.tyjcwt"><span style="font-size:12.0pt;color:black">Kirmani<i> et al.</i>, 1999</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">), a third goal was to show the
robustness of product line range effects on extension evaluations regardless of
parent brand concept.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><b><i><span lang="EN-IN" style="font-size:12.0pt;color:black">Design And
Procedure</span></i></b></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">One hundred and
92 university students were recruited to take part in an online study on
consumer perceptions of new products in exchange for course credit.
Participants were randomly assigned to the conditions of a 2 (range: wide vs.
narrow) x 2 (extension direction: up vs. down) x 2 (brand concept: prestige vs.
functional) between-subjects factorial design. Participants were given a
scenario in which they were asked to consider that a watch brand (Swatch vs.
Rolex) was introducing a new upscale or downscale product. Extension
information provided was limited to a brief description and price information.
To achieve the proposed goals, end prices were kept constant across conditions
while the average price varied in each condition such that the narrow range
would have a higher price average than the wide range in the upscale scenario
whereas the narrow range would have a lower price average than the wide range
in the downscale scenario.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">To
illustrate, participants in the upscale extension condition of a functional
brand either saw a wide price range ($99 to $499) or a narrow price range ($299
to $499) before they were presented with the extension information. Following
prior research (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:
&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.35nkun2"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Dall'Olmo Riley<i> et
al.</i>, 2013</span></a></span><span lang="EN-IN" style="font-size:12.0pt;
color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.tyjcwt"><span style="font-size:12.0pt;color:black">Kirmani<i> et al.</i>, 1999</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.lnxbz9"><span style="font-size:12.0pt;color:black">Lei<i> et al.</i>, 2008</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">), the upscale extension was
priced 50% above the highest end price ($749). On the other hand participants
in the downscale extension condition of a functional brand either saw a wide
price range ($99 to $499) or a narrow price range ($99 to $299) before they
were presented with a downscale extension was priced 50% below the lowest end
price ($66). All prices used in scenarios are presented in Appendix A.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><b><i><span lang="EN-IN" style="font-size:12.0pt;color:black">Measurement</span></i></b></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Extension
evaluations were measured by three items (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.lnxbz9"><span style="font-size:12.0pt;color:black">Lei<i> et al.</i>, 2008</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) assessing favorability (1 =
not at all, 7 = extremely), liking (1 = extremely negative, 7 = extremely
positive), and attractiveness (1 = not at all, 7 = extremely). These measures
were averaged to form a single composite score of attitude towards the
extension (Cronbach’s alpha =.85). Consumers’ willingness to purchase the new
extension product was measured with a single item scale (1 = very low, 7 = very
high) taken from Dodds<i> et al. </i>(</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3ygebqi"><span style="font-size:12.0pt;color:black">1991</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). Previous research indicates that more
knowledgeable consumers (experts) hold more complex cognitive structures,
better analytical capabilities, and improved ability to make elaborate
inferences whereas less knowledgeable consumers (novices) are more likely to
use extrinsic cues to evaluate a product (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2dlolyb"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Rao and Monroe, 1988</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). Prior research in vertical
line extensions (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.lnxbz9"><span style="font-size:12.0pt;color:black">Lei<i> et al.</i>, 2008</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) has shown that experts are
more likely to doubt the firm’s ability to deliver upscale extensions and to
have more confidence in the performance of downscale extensions compared to
novice consumers.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Because
varied levels of consumers’ knowledge can lead to different responses to
vertical extensions, prior knowledge is used as a control variable. Three items
adapted from Lei <i>et al. </i>(2008) were used to measure participants’
self-assessed knowledge about the point-and-shoot digital camera category.
Specifically, participants were asked: “I am ______________ with wristwatches”;
“Among my circle of friends, I think I know_________ about wristwatches”; and,
“I am ______________ with wristwatches prices”. All items were measure on
seven-point scales (1 = not at all knowledgeable, 7 = extremely knowledgeable).
Similarly, prior brand extension research (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.4f1mdlm"><span style="font-size:12.0pt;color:black">Milberg<i> et al.</i>, 2010</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) has shown that different
levels of brand familiarity can influence consumers evaluations of new
extension products. Therefore, a single item scale (1 = not at all familiar, 7
= extremely familiar) measure of brand familiarity taken from Milberg <i>et al.
</i>(2010) was used as a second control variable. Finally, participants
answered to questions that assessed their attitude towards the parent brand on
the same three item scales as those used for the extension evaluations before
answering a manipulation check question taken from Jun <i>et al.</i> (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.sqyw64"><span style="font-size:12.0pt;color:black">2005</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) that assessed perceived width of parent
brand’s price range on a seven-point scale (1 = extremely narrow, 7 = extremely
wide).</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><b><i><span lang="EN-IN" style="font-size:12.0pt;color:black">Results</span></i></b></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">A MANOVA on
extension judgments (attitude and willingness to buy) revealed significant main
effects of direction (F(2, 183) = 6.02, <i>p </i>&lt;.01), range (F(2, 183) =
4.00, <i>p </i>&lt;.05), and brand concept (F(2, 183) = 17.04, <i>p </i>&lt;.001);
significant two-way interaction of direction x range (F(2, 183) = 7.95, <i>p</i>
&lt;.000) and only a marginally significant two-way interaction of direction x
brand (F(2, 183) = 2.70 <i>p</i> &lt;.10). The main effects showed that
perceptions of Rolex were more favorable than those of Swatch, that a downward
stretch led to more favorable attitudes and purchase intentions than an upward
stretch, and that a wide price range generated more favorable extension ratings
than a parent brand with a narrow price range. However, the two-way interaction
of range x brand and the three-way interaction of direction x range x brand
were not significant, suggesting that price range influence vertical extension
ratings regardless of brand concept. Because these effects were non-significant,
brand concept results are not discussed any further and the proceeding results
discussion focuses on the lower order effects qualified by the two-way
interaction of direction and range which are examined using separate Animal
Navigation Oblique Vicious Aggregation (ANOVA) planned contrasts for each
dependent variable. Table 1 shows the cell means.</span></p>

<p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;
color:black">----Insert Table 1 here----<br>
<br>
<br>
</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">This
research argues that consumer evaluations of vertical extensions are influenced
by the width of a parent brand price structure. Planned contrasts showed that
in the upscale scenario, participants rated an upscale extension more favorably
when exposed to a wide price range than those that viewed a narrow price range
for both prestige (Attitude: M</span><sub><span lang="EN-IN" style="font-size:
7.0pt;color:black">w</span></sub><span lang="EN-IN" style="font-size:12.0pt;
color:black"> = 5.78 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;
color:black">n</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black">
= 4.98, t(184) = -2.28, <i>p</i> &lt;.05; Willingness to buy: M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">w</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 5.46 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">n</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 4.62, t(184) = -1.64, <i>p</i> =.104)
and functional brands (Attitude: M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">w</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 4.94 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">n</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 3.23, t(184) = 4.45, <i>p</i> &lt;.001;
Willingness to buy: M</span><sub><span lang="EN-IN" style="font-size:7.0pt;
color:black">w</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black">
= 3.73 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">n</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 2.14, t(184) = 3.01, <i>p</i>
&lt;.01). In contrast, there were no differences between the narrow and wide
conditions in participant ratings of downscale extensions (Attitude: M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">nd</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 4.75 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">wd</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 4.98; t(78) = -.76, <i>p &gt;</i>.10;
Willingness to buy: M</span><sub><span lang="EN-IN" style="font-size:7.0pt;
color:black">nd</span></sub><span lang="EN-IN" style="font-size:12.0pt;
color:black"> = 5.05 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;
color:black">wd</span></sub><span lang="EN-IN" style="font-size:12.0pt;
color:black"> = 4.76; t(78) =.62, <i>p &gt;</i>.10) which is consistent with
findings of the first experiment.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><b><i><span lang="EN-IN" style="font-size:12.0pt;color:black">Discussion</span></i></b></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Consistent with
predictions of the range theory, the data furnished by experiment 1 lends
support to the argument that consumers do not rely on a single parent brand
price anchor nor on the parent brand average price but rather on its price
range as reference to form judgments of upscale extensions. Regardless of brand
concept, this effect holds even when the narrow condition had a higher price
average than the wider price range condition. It is important to note that the
findings of experiment 1 do not suggest that a functional brand with lower
average and wider portfolio will be better evaluated than a prestige brand with
a narrow product line as shown by the non-significant brand x range
interaction. On the other hand, neither the price range account, the single
price anchor account, nor the price average account could explain how consumers
form judgments of downscale extensions. Alternatively, results are consistent
with the assumption that consumers form judgments of downscale extensions
regardless of parent brand price width because downscale extensions are of
lower risks.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">In
a vertical line extension context, previous research has shown that brand
expertise acts as a risk reduction mechanism that overcomes perceived
consistency effects when consumers judge downscale extensions (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1ksv4uv"><span style="font-size:12.0pt;color:black">Pontes<i> et al.</i>, 2017</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). The brand is perceived to be
credible because it has previously serviced a higher priced segment. In turn,
consumers’ confidence that the brand will meet expectations at lower price
tiers is enhanced. Therefore, the next study is designed to provide empirical
evidence for the hypothesis that perceived risk mediates the product line range
effect for upscale but not for downscale extensions due to perceptions of brand
credibility.</span></p>

<p class="MsoNormal" style="margin-bottom:12.0pt;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><br>
<br>
<br>
</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><b><span lang="EN-IN" style="font-size:12.0pt;color:black">Experiment 2</span></b></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><b><span lang="EN-IN" style="font-size:12.0pt;color:black">Design And
Procedure</span></b></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">One hundred U.S.
consumers (62% males) were recruited from Amazon Mechanical Turk to take part
in this online study on consumer perceptions of new products. Participants were
randomly assigned to the conditions of a 2 (range: wide vs. narrow) x 2 (extension
direction: upscale vs. downscale) between-subjects factorial design and given a
scenario in which they were asked to consider that a binocular brand (Bushnell)
was introducing a new upscale ($999) or downscale product ($24.98). Unlike the
first two experiments, price range was manipulated using a more complete
product line: 12 products in the wide condition and four products in the narrow
condition. This should allow participants to select which exemplars to use as
referents when judging the new extension product rather than relying
exclusively on endpoint prices.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">In
the wide range condition, participants saw a product line consisting of 12
products that ranged in price from $39.98 to $699.00 whereas in the narrow
range condition, participants viewed only four products. In the narrow upscale
condition prices ranged from $379 to $699 and in the narrow downscale condition
prices ranged from $39.98 to $129.98. Prices were based on the existing
Bushnell product portfolio range. Extension information provided was limited to
a brief description and price information. The four conditions and the full
price list shown to participants are demonstrated in Appendix B. <br>
<br>
<br>
</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><b><span lang="EN-IN" style="font-size:12.0pt;color:black">Measurement</span></b></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Extension
evaluation measures were similar to those used in experiment 1. Perceived risk
was operationalized by measuring performance risk, financial risk, and overall
risk with three seven-point Likert scales (1 = disagree; 7 = agree) that were
adapted from Ostrom and Iacobucci (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3cqmetx"><span style="font-size:12.0pt;color:black">1998</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). Specifically, participants were asked
to consider the following statements: “given the expense involved, purchasing
the new binoculars would be very risky”, “considering the potential problems
with the new product's performance, buying the new binoculars would be too
risky”, and “overall, buying the new binoculars would be very risky”. Brand
credibility has been broadly defined as the believability of an entity’s
intentions at a particular time to deliver what has been promised (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1rvwp1q"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Erdem and Swait, 2004</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). In a vertical extension
context, it reflects whether consumers trust that a brand is willing to and
capable of delivering the expected product benefits of an upscale or downscale
market segment. Therefore, two seven-point scale items (1 = not at all, 7 =
very) measuring perceived company’s capability and trustworthiness were used to
form a single measure of brand credibility (Cronbach’s alpha =.89). All other
measures were the same as the first two experiments.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><i><span lang="EN-IN" style="font-size:12.0pt;color:black">Results</span></i></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><i><span lang="EN-IN" style="font-size:12.0pt;color:black">Manipulation
check</span></i><span lang="EN-IN" style="font-size:12.0pt;color:black">. Price
range was once more successfully manipulated such that in the upscale scenario
participants evaluations of price range were significantly lower in the narrow
(M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">nu</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 3.71) than in the wide
condition (M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">wu</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 5.12; F(1, 47) = 13.58, <i>p
&lt;</i>.01). Likewise, in the downscale scenario, evaluations were
significantly lower in the narrow (M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">nd</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 4.27) condition than in the wide
condition (M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">wd</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 5.88; F(1, 47) = 30.41, <i>p
&lt;</i>.001).</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><i><span lang="EN-IN" style="font-size:12.0pt;
color:black">Extension Evaluations</span></i><span lang="EN-IN" style="font-size:
12.0pt;color:black">. This research argues that consumer evaluation of vertical
extensions is a function of the parent brand price range such that a wide
product line should lead to more favorable extension ratings than a narrow
product line. Consistent with findings of previous experiments, results from a
two-way ANOVA on extension attitudes shows an interaction effect of direction x
range (F(1, 96) = 4.70, <i>p &lt;</i>.05) as well as a main effect of direction
(F(1, 96) = 10.41, <i>p &lt;</i>.01). Further, main effects were found for
direction (F(1, 96) = 14.42, <i>p &lt;</i>.001) and range (F(1, 96) = 3.77, <i>p
=</i>.055) on willingness to buy. No other effects were significant. Planned
contrasts show that ratings of upscale extensions were more favorable in the
wide than the narrow condition (Attitude: M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">wu</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 4.13 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">nu</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 3.31; F(1, 47) = 7.14, <i>p =</i>.01;
Willingness to buy: M</span><sub><span lang="EN-IN" style="font-size:7.0pt;
color:black">wd</span></sub><span lang="EN-IN" style="font-size:12.0pt;
color:black"> = 3.17 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;
color:black">nd</span></sub><span lang="EN-IN" style="font-size:12.0pt;
color:black"> = 2.29; F(1, 47) = 3.57, <i>p &lt;</i>.10). However, there was no
significant differences in extension evaluations in the downscale scenario
(Attitude: M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">wd</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 4.36 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">nd</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 4.54; F(1, 51) =.27, <i>p &gt;</i>.10;
Willingness to buy: M</span><sub><span lang="EN-IN" style="font-size:7.0pt;
color:black">wd</span></sub><span lang="EN-IN" style="font-size:12.0pt;
color:black"> = 4.15 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;
color:black">nd</span></sub><span lang="EN-IN" style="font-size:12.0pt;
color:black"> = 3.77; F(1, 51) =.72, <i>p &gt;</i>.10).</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><i><span lang="EN-IN" style="font-size:12.0pt;
color:black">Perceived Risk. </span></i><span lang="EN-IN" style="font-size:12.0pt;
color:black">A two-way ANOVA on perceived risks reveal significant main effects
for Direction (F(1, 96) = 71.06, <i>p &lt;</i>.001) and range (F(1, 96) = 4.25,
<i>p &lt;</i>.05) as well as a significant interaction effect of direction x
range (F(1, 96) = 4.76, <i>p &lt;</i>.05). Consistent with prior research (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.lnxbz9"><span style="font-size:12.0pt;color:black">Lei<i> et al.</i>, 2008</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">), perceived risk ratings was
higher for upscale extensions than downscale extensions (M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">up</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 5.08 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">down</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 3.12; F(1, 96) = 71.06, <i>p &lt;</i>.001).
Further, planned contrasts reveal that perceived risks of upscale extensions
were higher for those participants in the narrow price range condition compared
to those that viewed the wide price range condition (M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">upN</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 5.67 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">upW</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 4.64; t(46) = -3.14, <i>p
&lt;</i>.01). On the other hand, there was no difference in perceived risk
ratings between narrow and wide price range conditions of downscale extensions
(M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">downN</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 3.24 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">downW</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 3.27; t(50) = -.08, <i>p
&gt;</i>.10).</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><i><span lang="EN-IN" style="font-size:12.0pt;
color:black">Mediation. </span></i><span lang="EN-IN" style="font-size:12.0pt;
color:black">Next, the hypothesis that perceived risk mediates the effect of
price range on extension evaluations for the upscale but not for the downscale
condition was tested using the PROCESS macro (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.4bvk7pj"><span style="font-size:12.0pt;color:black">model 8; Hayes, 2013</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). The narrow price range was
coded as zero and the wide price range condition as one. The downscale
condition was coded as minus one (-1) and the downscale condition was coded as
one (1). Brand credibility was added as a covariate. Age, gender, and income were
used as control variables. The range x direction interaction negatively
influenced perceptions of risk (<i>b</i> = -.58, <i>t</i>(92) = -2.58, <i>p</i>
&lt;.05), which in turn negatively influenced extension evaluations (<i>b</i> =
-.42, <i>t</i>(91) = -4.29, <i>p</i> &lt;.001). The indirect effect of the
price range x extension direction on extension evaluations through perceived
risk was 0.48 (95% CI:.1048 to 1.0236), providing support for the
moderated-mediation hypothesis.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Specifically,
in the upscale scenario, there was a significant indirect path that flows from
price range to perceived risk to extension evaluation (<i>b</i> =.41, 95%
CI:.1453 to.7751). On the other hand, there was no evidence that price range
influenced extension evaluations through perceived risk for downscale
extensions (95% CI: -.3967 to.2156), supporting H</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">2a</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black">. Rather, regression results reveal that
brand credibility negatively influenced perceptions of risk (<i>b</i> = -.23, <i>t</i>(92)
= -1.90, <i>p</i> =.061) and that both perceived risks (<i>b</i> = -42, <i>t</i>(91)
= -4.29, <i>p</i> &lt;.001) and brand credibility (<i>b</i> =.28, <i>t</i>(91)
= 2.36, <i>p</i> &lt;.05) direclty influenced downscale extension evaluations.
Because there was no difference in ratings of perceived risk and brand
credibility (M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">downN</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 4.69 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">downW</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 5.02; t(50) = 1.27,<i> p </i>&gt;.10)
between wide and narrow conditions for downscale extensions, results show
similar ratings for extension evaluations. There was no evidence that price
range influenced extension evaluations directly (<i>b</i> =.07, t(91) =.31, <i>p</i>
&gt;.10).</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Discussion</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Experiment 2
replicates results found in the first study with a different manipulation that
allowed respondents the freedom to form their standard of comparison based on
more price cues (wide = 12, narrow = 4). Further, this study extends the
findings of experiment 1 by showing that the effect of price range on extension
evaluation is mediated by perceived risks. Specifically, the introduction of a
new upscale extension is perceived to be of higher risk when the parent brand
had a narrow price range compared to a wide price range. In turn, upscale
extensions were rated more favorably when the product line had a wide versus
narrow price range. On the other hand, the data presented for downscale
extensions shows that price range did not influence extension evaluations
indirectly through perceived risks. Rather, it provides support for the
argument that downscale extensions yield lower uncertainty because consumers
believe that the parent brand is able to deliver the expected benefits of lower
priced segments.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><b><span lang="EN-IN" style="font-size:12.0pt;color:black">Experiment 3</span></b></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">The goal of this
experiment is to demonstrate a boundary condition to the range effect on
upscale extensions. In the previous experiments, information provided to
participants did not mention competitor or category prices. Participants were,
therefore, likely to rely on parent brand prices as the frame of reference when
making judgments about upscale extensions. However, prior research has shown
that consumer preferences formed in isolation of competition tend to differ
from those judgments formed when competitors are present (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.vx1227"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Hsee and Leclerc, 1998</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3fwokq0"><span style="font-size:12.0pt;color:black">Hsee<i> et al.</i>, 1999</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1v1yuxt"><span style="font-size:12.0pt;color:black">Leclerc<i> et al.</i>, 2005</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). Thus, this experiment tests
whether the product line range effect holds for upscale extensions when
category prices are available, and in particular, when there is a shift in
reference price points from parent brand prices to product category prices.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Method</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">A total of 300
participants (61% females), all members of Amazon’s Mechanical Turk completed
this online study for a payment. Participants were randomly assigned to a 2
(product line range: wide vs. narrow) x 4 (attention focus: unspecific vs.
category end-points vs. product line end-points vs. control) between-subjects
design. Participants were asked to consider some information about a tire
manufacturer (BlueStreak) and prices in the tire category. Then, participants
were provided with a list of prices consisting of 22 products of the most
common brands and models of tires available in the market (see Appendix C),
including BlueStreak product line prices (wide = $89 to $299; narrow = $199 to
$299). In the control condition, participants were exposed to BlueStreak prices
only and there was no mention of competitor or category prices.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">To
manipulate attention focus, participants in the product line focus condition
were asked to recall the lowest (wide = $89; narrow = $199) and highest ($299)
price of BlueStreak. In the category end-points condition, participants were
asked to recall the lowest ($39) and the highest ($673) price of the tires
product category. In the unspecific focus condition, participants did not
receive any instructions to recall prices. The manipulation of attention focus
is based on previous research which shows that making one option perceptually
salient can activate a narrow focus by drawing attention to a particular item (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.2r0uhxc"><span style="font-size:12.0pt;color:black">Hamilton and Chernev, 2010</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1664s55"><span style="font-size:12.0pt;color:black">Hamilton<i> et al.</i>, 2007</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">)). After answering priming
questions, participants were asked to consider that BlueStreak was introducing
a new upscale product ($449) before turning to the dependent measures in the
next screen.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">In
addition to the measures used in experiment 1, this experiment also measured
perceptions of fit. Milberg <i>et al.</i> (2010) has demonstrated that when
competitors are present, consumer’s reliance on fit decreases because the focus
of attention broadens with the addition of information about competitors.
Therefore, it is expected that fit will be an important cue for those consumers
with a narrow focus on the parent brand. Conversely, there should be no effect
of fit on evaluations when consumers focus is unspecific, where consumers are
likely to take into consideration all category prices. Fit was measured using a
four-item scale adapted from Milberg <i>et al.</i> (1997) that asked
participants to rate the new upscale extension in terms of similarity,
consistency (1 = not at all, 7 = very), fit (1 = low fit, 7 = high fit), and
whether introducing the new upscale extension made sense (1= little sense, 7= a
lot of sense). Finally, two seven-point scale items (1 = extremely narrow, 7 =
extremely wide) were used as manipulation checks that measured participants’
perceptions of parent brand price range and category price range.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><i><span lang="EN-IN" style="font-size:12.0pt;color:black">Results</span></i></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Manipulations
worked as predicted. Participants ratings of product line price range were
significantly lower in the narrow (M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">n</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 3.61) than in the wide condition (M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">w</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 4.88; F(1, 298) = 60.94, <i>p &lt;</i>.001)
but there were no differences in perceptions of category price range between
narrow (M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">n</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 6.56) and wide product line
conditions (M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">wd</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 6.52; F(1, 298) =.25, NS).
Results of a 2 x 4 ANOVA revealed a significant interaction effect of product
line range by attention focus on extension evaluations (interaction: F(3, 294)
= 2.44, <i>p</i> =.064) as well as a main effect of product line range (F(1,
292) = 3.83, <i>p</i> =.051). There were no other significant effects.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Subsequent
2 (product line range) x 2 (unspecific focus vs. focus on category end-points)
ANOVA analyses reveal no significant interaction effects (<i>p</i> &gt;.90)
suggesting that those who were not primed to focus their attention on category
end prices, naturally do so. Participants in both conditions, built a broader
frame of reference based on category end prices to make judgments about the
upscale extension. In similar fashion, the interaction effect of product line
range x attention focus (product line end-points vs. control) was also not
significant (<i>p</i> &gt;.90), showing that participants who were asked to
focus on product line prices had a narrow focus. Just like those that were
exposed to parent brand prices only, participants primed to focus on product
line prices made judgments about the extension based on the parent brand as a
frame of reference, regardless of the presence of competitor’s prices in the
category.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">In
support, pairwise comparisons show that respondents rated the new upscale
extension more favorably in the wide versus the narrow price range (M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">n</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 3.55 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">w</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 4.32; F(1/292) = 5.26, <i>p</i>
&lt;.05) in a separate evaluation mode, where participants viewed parent brand
prices only, as well as in a joint evaluation mode (M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">n</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 3.44 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">w</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 4.22; F(1/292) = 5.60, <i>p</i>
&lt;.05), where they were asked to narrow their focus by recalling the highest
and lowest price of BlueStreak’s product line. On the other hand, there was no
difference in evaluations of the new upscale extension between narrow and wide
product line range conditions when participants were primed to think
specifically about category end prices (M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">n</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 4.12 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">w</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 4.01; F(1/292) =.10, NS) or when they
were free to choose any prices as standard of comparison (M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">n</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 3.89 vs. M</span><sub><span lang="EN-IN" style="font-size:7.0pt;color:black">w</span></sub><span lang="EN-IN" style="font-size:12.0pt;color:black"> = 3.75; F(1/292) =.18, NS). These results
are graphically presented in Figure 2.</span></p>

<p class="MsoNormal" align="center" style="margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;text-indent:.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">----Insert Figure 2 here----</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><i><span lang="EN-IN" style="font-size:12.0pt;
color:black">Mediation</span></i><span lang="EN-IN" style="font-size:12.0pt;
color:black">. The assumption that consumers rely on perceptions of fit when
having a narrow focus on the parent brand is tested using the PROCESS macro (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.4bvk7pj"><span style="font-size:12.0pt;color:black">model 8; Hayes, 2013</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). The narrow price range was
coded as zero and the wide price range condition as one. Because there was no
interaction between product line range and the two narrow focus conditions, the
product line focus and the control conditions were collapsed and coded as zero.
Likewise, since there was no interaction between product line range and the two
broad focus conditions, the category end-points and the unspecific focus
conditions were collapsed and coded as one.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Results
from the moderated-mediation analysis reveal that the range x focus interaction
negatively influenced perceptions of fit (<i>b</i> = -.76, <i>t</i>(296) =
-2.67, <i>p</i> &lt;.01), which in turn positively influenced extension
evaluations (<i>b</i> =.61, <i>t</i>(295) = 10.73, <i>p</i> &lt;.001). The
indirect effect of the price range x extension direction on extension
evaluations through perceived risk was -0.47 (95% CI: -.8310 to -.1265).
Specifically, when consumers had a focus on parent brand prices, there was a
significant indirect path that flows from product line range to perceived fit
to extension evaluation (<i>b</i> =.31, 95% CI:.0599 to.5767). The direct
effect was also significant (<i>b</i> =.47, 95% CI:.0717 to.8610) when
consumers used parent brand prices as a frame of reference. On the other hand,
there was no evidence that price range influenced extension evaluations
directly (95% CI: -.3563 to .4271) or indirectly, through perceived fit (95%
CI: -.3980 to .0841), when consumers focused on category end prices.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><i><span lang="EN-IN" style="font-size:12.0pt;color:black">Discussion</span></i></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">This
experiment yields two important conclusions. First, it provides further
evidence for the proposed product line range effect in upscale extensions
showing that this effect holds when category price information is available as
long as consumers have a narrow focus of attention which leads to a greater
reliance on perceived fit between parent brand and extension to form judgments
about the new product. Second, this study demonstrates an important boundary
condition to the product line range effect. When consumers have a broader
attention focus, parent brand prices become less diagnostic of upscale
extension judgments as consumers’ attention is drawn to the endpoint prices in
the product category.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><b><span lang="EN-IN" style="font-size:12.0pt;color:black">General
Discussion</span></b></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">In the context of
vertical line extensions, understanding how consumer use parent brand prices as
reference is particularly important because product line prices have the
special role of signaling consumers how consistent a vertical extension may be
with its parent brand. Yet, to the best of our knowledge, this research is the
first to investigate how current product line price range influence consumer
judgments of vertical line extensions. Contrary to prototypical models of price
categorization (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:
&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.49x2ik5"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Helson, 1947</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">), this research shows that
consumer evaluations of upscale extensions are a function of parent brand
endpoint prices. In line with predictions of the range theory (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3j2qqm3"><span style="font-size:12.0pt;color:black">Volkmann, 1951</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">), upscale extensions were
judged more favorably when participants were presented with a wide versus a
narrow price range. Regardless of brand concept, this effect holds even when
the narrow condition had a higher price average than the wider price range
condition, challenging previous research that suggest managers to avoid having
lower-end models in order to keep brand equity high (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.3q5sasy"><span style="font-size:12.0pt;color:black">Randall<i> et al.</i>, 1998</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). It has been argued that
low-end products may raise questions in the consumers’ mind about the
possibility that some of the lesser quality components are also used in the
high-end products. However, consistent with research showing that introducing
downscale extensions improve perceptions of brand innovativeness (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1egqt2p"><span style="font-size:12.0pt;color:black">Heath<i> et al.</i>, 2011</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">), this research shows that
low-end products can also positively influence consumers’ acceptance of new
upscale product.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Furthermore,
it was shown that the product line range effect was mediated by perceived
consistency and perceived risk for upscale but not for downscale extensions
such that upscale extensions were perceived to be more consistent with its
parent brand and as of lower risk when presented in the context of a wide
versus a narrow price range. The range effect disappears, however, when
consumers have a broad focus in which attention shifts to category endpoint
prices, making parent brand prices less diagnostic of upscale extension
judgments. Finally, neither the price range account, nor the single price
anchor account, nor the price average account could explain how consumers form
judgments of downscale extensions. Alternatively, results show a null effect of
price range on consumer ratings of downscale extensions. This finding is
consistent with previous literature showing that downscale extensions are
likely to have a neutral effect on the parent brand (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1egqt2p"><span style="font-size:12.0pt;color:black">Heath<i> et al.</i>, 2011</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) and that price magnitude (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.35nkun2"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Dall'Olmo Riley<i> et
al.</i>, 2013</span></a></span><span lang="EN-IN" style="font-size:12.0pt;
color:black">) and perceived consistency (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1ksv4uv"><span style="font-size:12.0pt;color:black">Pontes<i> et al.</i>, 2017</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) have little or no influence
on downscale extensions.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Previous
research has argued that downscale extensions yield lower uncertainty because
they do not signal a change in brand expertise levels once the highest price
endpoint is kept unchanged (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1egqt2p"><span style="font-size:12.0pt;color:black">Heath<i> et al.</i>, 2011</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1ksv4uv"><span style="font-size:12.0pt;color:black">Pontes<i> et al.</i>, 2017</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). Although it is correct to
say that changes in expertise elicit higher perceived risks in upscale
extensions, stating that a lack of change in expertise levels will lead to
lower levels of risk in downscale conditions is inaccurate. In the current
research, the highest price endpoint was always higher in the wide than in the
narrow condition. If assumptions of past research were correct, ratings of
brand expertise should have been higher in the wide versus the narrow
condition, resulting in a product line range effect also in downscale
extensions. Rather, findings from all studies consistently show no difference
between wide and narrow price range conditions.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">It
seems that the mechanisms affecting how consumers use price cues to infer brand
expertise, and in turn brand credibility, are not the same as those used by
consumers when forming perceptions of consistency between extension and product
line. Grounded on adaptation-level theory (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.49x2ik5"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Helson, 1947</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) and consistent with the
notion that consumers judge prices categorically (e.g., “good” vs. “bad”;
“large” vs. “medium” vs. “small”), it is possible that when consumers make
judgments of downscale extensions they use parent brand prices to establish a
simplified brand expertise adaptation-level, such as “<i>higher than extension</i>”.
Thus, rather than comparing downscale extensions with a single price point or
to product line endpoint prices, consumers judge the extension according to its
position (higher vs. lower) relative to the perceived brand credibility level.
In this case, consumers should be confident that the brand can deliver the
expected benefits of lower priced segments because they perceive the brand as
having higher than required brand credibility when introducing downscale
extensions.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><i><span lang="EN-IN" style="font-size:12.0pt;color:black"><br>
<br>
<br>
</span></i></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><i><span lang="EN-IN" style="font-size:12.0pt;color:black">Managerial
Implications</span></i></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">The research
presented in this article offers important insights for marketing managers that
want to leverage their product line by introducing vertical extensions. As it
was shown, consumers evaluations of vertical extensions act as a function of
the parent brand price range. Managers that want to introduce a new upscale
product should look not only for anchoring and consistency effects of the
highest endpoint price in the existing product line but also understand how
low-end prices influence consumer judgments towards upscale extensions. In this
sense, managers should consider reducing prices of low-end products and/or
extend product range display when stretching a product line upwards.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">To
illustrate, examining Amazon’s U.S. website, more than 80 bread makers from
more than 20 brands with prices ranging from $40 to $1148 were found (prices in
US$). Sunbeam and West Brand are at the lower-end of this range offering
products from $50 to $80 while the Panasonic line of products ranges from $135
to $335. Now, consider that Panasonic is introducing a new upscale bread maker
extension priced at $450. The finding that price range affects perceptions of
new products suggests that managers can improve favorability of this product by
reducing the price of its low-end products. By making Panasonic an alternative for
the lower price segment ($50 to $80), unit sales should increase because
consumers tend to prefer a low-ranked product of a higher quality brand to a
high-ranked product of a lower quality brand when they are evaluated together (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1v1yuxt"><span style="font-size:12.0pt;color:black">Leclerc<i> et al.</i>, 2005</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). At the same time, Panasonic
has the potential to earn larger margins by improving its chance to
successfully introduce an upscale product. That said, companies must use
caution when applying the aforementioned product line pricing strategies. Prior
research shows that introducing downscale extensions or reducing prices of
lower-end models may reduce perceptions of brand prestige (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.1egqt2p"><span style="font-size:12.0pt;color:black">Heath<i> et al.</i>, 2011</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">) and affect negatively owners’
attitudes toward the brand (</span><span lang="EN-IN" style="font-size:12.0pt;
font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.tyjcwt"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Kirmani<i> et al.</i>,
1999</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">).
Therefore, managers should weigh the benefits of attracting new customers
against the costs of alienating current customers.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Furthermore,
as this research shows, the product line range effect is more likely to be
observed when consumers have a narrow focus of attention. Therefore, in a
single brand retail setting (e.g., Apple, Nike, Lacoste) where consumers are
exposed to products belonging to one parent brand, there is a natural focus on
product line prices. In this case, brands can improve perceptions of new
products by displaying a wider price range, facilitating new product
acceptance. In a multi-brand retail setting (e.g., department stores), the
likelihood that consumers will rely on parent brand prices decreases whereas
reliance on category endpoint prices tend to increase consumers’ broader
attention focus.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><i><span lang="EN-IN" style="font-size:12.0pt;color:black">Limitations
And Future Research</span></i></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">As with any
study, this research has several limitations which are noted and taken as
points for future direction. First, results presented in this research may be
limited to the specific characteristics of durable products (e.g., binoculars,
cameras, tires). Given that some product categories depend more on price cues
to infer quality than others (Zeithaml, 1988), future research could examine
whether these results extend to a service setting and to a store brands
context. Second, all manipulations included brands with multiple products in a
line. Prior research examining single product brands has shown that consumers
hold very strong expectations for their level of quality (</span><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.25b2l0r"><span style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;color:black">Palmeira, 2014</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.kgcv8k"><span style="font-size:12.0pt;color:black">Palmeira and Thomas, 2011</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). Therefore, future research
could contrast lines with only one and two or products. It could be argued, however,
that a single product brand is the ultimate narrow range where the highest and
lowest price are the same. In that sense, consumers should perceive a price
increase to be much higher when a new upscale product is introduced for a
single product in comparison to a multi-product line.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Finally,
it should also be acknowledged that the use of a student sample to investigate
luxury brands (experiment 1) limits the generalization of results since
students are not the typical consumer of luxury products. Although, many
university students may be from privileged families where the access to luxury
products is not unusual and it is possible that participants in this study were
in fact consumers of luxury products, the pattern of results resemble more
closely non-owners attitudes to luxury brands (</span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.26in1rg"><span style="font-size:12.0pt;color:black">Fu<i> et al.</i>, 2009</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">; </span><span lang="EN-IN"><a href="https://docs.google.com/document/d/1g91rnylmi7ZxXPgaP1Z9QB_3-9XEd-E7/edit#heading=h.tyjcwt"><span style="font-size:12.0pt;color:black">Kirmani<i> et al.</i>, 1999</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black">). Therefore, generalization of
findings regarding luxury brands should be done with care and based on the
premise that owners and non-owners respond differently to vertical line
stretches of luxury brands.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;font-family:
&quot;Times New Roman&quot;,&quot;serif&quot;;color:black">As part of the </span><span lang="EN-IN"><a href="https://osf.io/e81xl/wiki/home/"><span style="font-size:12.0pt;
font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;color:blue">Reproducibility Project:
Cancer Biology</span></a></span><span lang="EN-IN" style="font-size:12.0pt;
font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;color:black"> we published a Registered
Report that described how we intended to replicate selected experiments from
the paper “Coding-independent regulation of the tumor suppressor PTEN by
competing endogenous mRNAs”. Here we report the results. We found depletion of
putative PTEN competing endogenous mRNAs (ceRNAs) in DU145 cells did not impact
<i>PTEN</i> 3’UTR regulation using a reporter, while the original study
reported decreased activity when <i>SERINC1</i>, <i>VAPA</i>, and <i>CNOT6L</i>
were depleted. Using the same reporter we found decreased activity when ceRNA
3’UTRs were overexpressed, while the original study reported increased
activity. In HCT116 cells, ceRNA depletion resulted in decreased PTEN protein
levels, a result similar to the findings reported in the original study;
however, while the original study reported an attenuated ceRNA effect in microRNA
deficient (Dicer</span><sup><span lang="EN-IN" style="font-size:7.0pt;font-family:
&quot;Times New Roman&quot;,&quot;serif&quot;;color:black">Ex5</span></sup><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;color:black">)
HCT116 cells, we observed increased PTEN protein levels. Further, we found
depletion of the ceRNAs <i>VAPA</i> or <i>CNOT6L</i> did not statistically
impact DU145, wild-type HCT116, or Dicer</span><sup><span lang="EN-IN" style="font-size:7.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;color:black">Ex5</span></sup><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;
color:black"> HCT116 cell proliferation. The original study reported increased
DU145 and wild-type HCT116 cell proliferation when these ceRNAs were depleted,
which was attenuated in the Dicer</span><sup><span lang="EN-IN" style="font-size:
7.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;color:black">Ex5</span></sup><span lang="EN-IN" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;
color:black"> HCT116 cells. Differences between the original study and this
replication attempt, such as variance between biological repeats, are factors
that might have influenced the results. Finally, we report meta-analyses for
each result.</span></p>

<p class="MsoNormal" style="margin-bottom:0in;margin-bottom:.0001pt;text-indent:
.5in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;font-family:
&quot;Times New Roman&quot;,&quot;serif&quot;;color:black"><br>
</span><span lang="EN-IN" style="font-size:12.0pt;color:black">This research
shows that upscale extensions are judged more favourably in the context of a
wide versus a narrow product line even when the highest endpoint in both
product lines are equally close to the extension and that this effect is
mediated by perceived consistency and perceived risk. The range effect
disappears, however, when consumers have a broad focus in which attention
shifts to category endpoint prices, making parent brand prices less diagnostic
of upscale extension judgments.</span></p>

<p class="MsoNormal" style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;
margin-left:0in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;
color:black">The </span><span lang="EN-IN"><a href="https://osf.io/e81xl/wiki/home/"><span style="font-size:12.0pt;
color:blue">Reproducibility Project: Cancer Biology</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black"> (RP:CB) is a collaboration
between the </span><span lang="EN-IN"><a href="https://cos.io/"><span style="font-size:12.0pt;color:blue">Center for Open Science</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black"> and </span><span lang="EN-IN"><a href="https://www.scienceexchange.com/"><span style="font-size:12.0pt;
color:blue">Science Exchange</span></a></span><span lang="EN-IN" style="font-size:12.0pt;color:black"> that seeks to address concerns about
reproducibility in scientific research by conducting replications of selected
experiments from a number of high-profile papers in the field of cancer biology
(Errington et al., 2014). For each of these papers a Registered Report
detailing the proposed experimental designs and protocols for the replications
was peer reviewed and published prior to data collection. The present paper is
a Replication Study that reports the results of the replication experiments
detailed in the Registered Report (Phelps et al., 2016) for a paper by Tay et
al. (2011) and uses a number of approaches to compare the outcomes of the
original experiments and the replications.</span><sup><span lang="EN-IN" style="font-size:7.0pt;color:black">1,2</span></sup></p>

<p class="MsoNormal" style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;
margin-left:0in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;
color:black">In 2011, Tay et al. reported <i>PTEN</i> was modulated through
competing endogenous RNAs (ceRNAs), which are protein-coding RNA transcripts
that compete for microRNAs through common micoRNA response elements (MREs).
Testing four candidate <i>PTEN</i> ceRNAs Tay and colleagues reported that for
three of these candidate ceRNAS (<i>SERINC1</i>, <i>VAPA</i>, and <i>CNOT6L</i>)
silencing the (<i>R</i></span><i><sup><span lang="EN-IN" style="font-size:7.0pt;
color:black">2 </span></sup></i><span lang="EN-IN" style="font-size:12.0pt;
color:black">= 0.01). ceRNAs impacted the activity of a luciferase construct
engineered with the 3’UTR of <i>PTEN</i> (Tay et al., 2011). Using the same
reporter HEK 293T construct, overexpression of the 3’UTRs of these CERNAs were
reported to increase luciferase activity suggesting inhibition of <i>PTEN</i>
3’UTR was HEK 293T relieved (Tay et al., 2011). These effects were reported to
be dependent on microRNAs since inhibition of PTEN protein expression when
ceRNAs were depleted was abrogated when DICER, a key part of the microRNA
machinery, was disrupted (Tay et al., 2011). Two ceRNAs, <i>VAPA</i> and <i>CNOT6L</i>,
when depleted also resulted in increased cell proliferation and phosphorylation
of AKT when depleted, which was attenuated when DICER was disrupted (Tay et
al., 2011), (<i>R</i></span><i><sup><span lang="EN-IN" style="font-size:7.0pt;
color:black">2 </span></sup></i><span lang="EN-IN" style="font-size:12.0pt;
color:black">= 0.01) [1-5].</span></p>

<p class="MsoNormal" style="margin-top:9.0pt;margin-right:0in;margin-bottom:9.0pt;
margin-left:0in;line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;
color:black">The Registered Report for the paper by Tay et al. (2011) described
the experiments to be replicated (Figures 3C-D, 3G-H, 5A-B, and Supplemental
Figures S3A-B), and summarized the current evidence for these findings (Phelps
et al., 2016). Since that publication additional studies have reported finding
other ceRNAs of <i>PTEN</i>. <i>TNRC6B</i> was identified as a <i>ceRNA</i> of <i>PTEN</i>
with depletion of <i>TNRC6B</i> reported to decrease PTEN mRNA and protein
expression in the prostate cancer cell lines DU145, 22RV1, and BM1604 and
increase cell proliferation in DU145 and PC3 cells (Zarringhalam et al., 2017).
Zarringhalam et al. (2017) also used <i>CNOT6L</i> in their study and reported
depletion of <i>CNOT6L</i> produced similar results as <i>TNRC6B</i> depletion.
<i>DNMT3B</i> and <i>TET3</i> were recently reported to be ceRNAs of <i>PTEN</i>
with miR-4465 identified as a microRNA regulating these three transcripts via
their 3’UTRs (Roquid et al., 2019). Multiple studies are reporting a growing
list of potential ceRNAs, which includes mRNAs, pseudogenes, lncRNAs, and
circRNAs (Gebert and MacRae, 2019; Li et al., 2018; Wang et al., 2016; Yang et
al., 2016). For example, the lncRNA <i>BGL3</i> has been identified as a ceRNA
for <i>PTEN</i> to regulate Bcr-Abl-mediated cellular transformation in chronic
myeloid leukemia (Guo et al., 2015) and c-Myc has been reported as a potential
ceRNA for PML/RARα in acute promyelocytic leukemia (Ding et al., 2016).
Prediction of putative ceRNAs are being reported using a variety of
computational methods and data sources that construct ceRNA interaction
networks (Chen et al., 2018; Chiu et al., 2017; Feng et al., 2019; Liu et al.,
2019; Park et al., 2018b; Song et al., 2016; Sun et al., 2016; Swain and
Mallick, 2018; Wang et al., 2019; Yue et al., 2019). Additionally, recently it
has been reported that 3’UTR shortening of transcripts of predicted ceRNAs
could be a potential mechanism of repressing tumor-suppressor genes, including <i>PTEN</i>,
in trans by disrupting ceRNA cross-talk (Park et al., 2018a).</span></p>

<p class="MsoNormal" style="margin-top:12.0pt;margin-right:0in;margin-bottom:
3.0pt;margin-left:0in;text-align:justify;line-height:normal"><b><span lang="EN-IN" style="font-size:16.0pt;color:black">References</span></b></p>

<p class="MsoNormal" style="line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">McCauley, S. M., &amp; Christiansen, M. H.
(2019) Language Learning as Language Use: A Cross-Linguistic Model of Child
Language Development. Psychological Review, 126(1): 1-51.
https://doi.org/10.1037/rev0000126</span></p>

<p class="MsoNormal" style="line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">De Vries R, Nieuwenhuijze M, Buitendijk
SE, &amp; the members of Midwifery Science Work Group. (2013) What does it take
to have a strong and independent profession of midwifery? Lessons from the
Netherlands. Midwifery, 29(10): 1122-1128.</span></p>

<p class="MsoNormal" style="line-height:normal"><span lang="EN-IN" style="font-size:12.0pt;color:black">Kalnay, E., Kanamitsu, M., Kistler, R.,
Collins, W., Deaven, D., Gandin, L., Iredell, M., Saha, S., White, G., Woollen,
J., Zhu, Y., Chelliah, M., Ebisuzaki, W., Higgins, W., Janowiak, J., Mo, K.C.,
Ropelewski, C., Wang, J., Leetmaa, A., ... Joseph, D. (1996) The NCEP/NCAR
40-year reanalysis project. Bulletin of the American Meteorological Society
77(3), 437-471.</span></p>

<p class="MsoNormal"><span lang="EN-IN">&nbsp;</span></p>

</div>




</body></html>
`;
const text = convert(html, {
  wordwrap: 130,
});
// console.log(text); // Hello World

const fs = require("fs");


fs.writeFile("./test1.txt", text, (err) => {
  if (err) {
    console.error(err);
  }
  // file written successfully
});
