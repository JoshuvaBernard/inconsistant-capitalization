// Create Objects
const apples = {name: 'Apples', age:'12'};
const bananas = {name: 'Bananas'};
const oranges = {name: 'Oranges'};

// Create a Map
const fruits = new Map();

// Add new Elements to the Map
fruits.set(500, apples);
fruits.set(bananas, 300);
fruits.set(oranges, 200);

// fruits.get(apples)
console.log(fruits.get(500).age);