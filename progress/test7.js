// un-sorted
// tried this method it takes 8 seconds


const start = Date.now();
const data_map = new Map();
var JSSoup = require("jssoup").default;
const fs = require("fs");

const html = fs.readFileSync("./HTMLTestFile.htm", {
  encoding: "utf8",
  flag: "r",
});

var soup = new JSSoup(html);
var paragraph = soup.find("p");

let no_of_paragraph = 0;

function caseCheck(current, previous) {
  if (current === previous) {
    return 1;
  } else if (current.toLowerCase() === previous.toLowerCase()) {
    return 2;
  } else {
    return 0;
  }
}

while (paragraph) {
  //   console.log("\n\n\npara no - " + no_of_paragraph);
  //   console.log(paragraph.text);
  // filter out unwanted

  let para_content = paragraph.text.split(/[,\s\n]/).filter(function (mov) {
    const pattern = /^[a-zA-Z]+$/;
    let result = pattern.test(mov);
    return result;
  });

  //   console.log(para_content);

  // try adding the filter within this for loop
  for (let i = 0; i < para_content.length; i++) {
    let j;
    data_map.set(`p${no_of_paragraph} : ${i}`, para_content[i]);
  }

  paragraph = paragraph.nextSibling;
  no_of_paragraph++;
}

let actual_accurance = [];
let inconsistant_word;
let actual_word;
let inconsistant_accurance = [];
let flag = 0;

let final_obj = [];

data_map.forEach(function (value1, key1) {
  //////////////////////////////////////////////////////////////////
  data_map.forEach(function (value2, key2) {
    // console.log(key1, key2);
    if (key1 != key2) {
      if (caseCheck(value1, value2) == 1) {
        actual_accurance.push(key1);
        actual_word = value1;
      } else if (caseCheck(value1, value2) == 2) {
        flag = 1;
        inconsistant_accurance.push(key2);
        inconsistant_word = value2;
      }
    }
  });
  if (flag) {
    let temp_obj = {
      real_word1: actual_word,
      actual_accurance1: actual_accurance,
      inconsistant_word1: inconsistant_word,
      inconsistant_accurance1: inconsistant_accurance,
    };
    final_obj.push(temp_obj);
    flag = 0;
    actual_accurance = [];
    inconsistant_word = "";
    actual_word = "";
    inconsistant_accurance = [];
  }
});

// const obj_data = Object.fromEntries(sorted_data_map);

const myJSON = JSON.stringify(final_obj);
// console.log(myJSON);
fs.writeFile("./test6.txt", myJSON, (err) => {
  if (err) {
    console.error(err);
  }
});

const stop = Date.now();
console.log(`Time Taken to execute = ${(stop - start) / 1000} seconds`);
