const start = Date.now();
console.time("codezup");
const { convert } = require("html-to-text");
const fs = require("fs");

// There is also an alias to `convert` called `htmlToText`.

const html = fs.readFileSync("./HTMLTestFile.htm", {
  encoding: "utf8",
  flag: "r",
});
// console.timeEnd("codezup");

const text = convert(html, {
  wordwrap: 130,
});
// console.log(text.split(" ")); // Hello World
// text.toLowerCase();
// let text_arr = text.split(/[,\s\n]/).filter((element) => element);

// add map method
let text_arr = text.split(/[,\s\n]/).filter(function (mov) {
  const pattern = /^[a-zA-Z]+$/;
  let result = pattern.test(mov);
  return result;
});

//assigning jsons
// const obj = Object.fromEntries(
//   text_arr.map((mov, index) => [
//     {
//       word: mov,
//       position: index,
//     },
//   ])
// );
const obj = [];
for (let i = 0; i < text_arr.length; i++) {
  let temp = {
    word: text_arr[i],
    index: i,
  };
  obj.push(temp);
}

// console.log(obj);

// let text_sorted = obj.sort((a, b) => {
// 	if (a.firstName < b.firstName) {
// 	  return -1;
// 	}
// 	if (a.firstName > b.firstName) {
// 	  return 1;
// 	}
// 	return 0;
//   });

// console.log(text_arr.sort());

// const text_sorted = text_arr;

// const text_sorted = text_arr.sort((a, b) => {
//   let fa = a.word.toLowerCase(),
//     fb = b.word.toLowerCase();

//   if (fa < fb) {
//     return -1;
//   }
//   if (fa > fb) {
//     return 1;
//   }
//   return 0;
// });

// const text_sorted = text_arr.sort((a, b) => {
//   return a.localeCompare(b, undefined, { sensitivity: "base" });
// });

const text_sorted = obj.sort((a, b) => {
  return a.word.localeCompare(b.word, undefined, { sensitivity: "base" });
});
console.log(text_sorted);
const myJSON = JSON.stringify(text_sorted);

fs.writeFile("./test2.txt", myJSON, (err) => {
  if (err) {
    console.error(err);
  }
  // file written successfully
});

const stop = Date.now();
console.timeEnd("codezup");
console.log(`Time Taken to execute = ${(stop - start) / 1000} seconds`);

/*

arr = [{word:"a", index:0}, {word:"A", index:1}, {word:"b", index:2}]
 result = sorted index =>  1 0 2

algorithm
	split text into array
	sort the array
	find in consistant cap
	if found search that word in text and note the index

questions
	how to seperate paragraphs? wrt \n or p tags?
	does a empty space or a newline count as a character?

	take a temp word;
	if same case true;
		store words and indexs
	different case false;
		store words and indexs
	different word break;

	next word is the new temp word;
*/
