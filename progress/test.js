const start = Date.now();
const { compile } = require("html-to-text");
const convert = compile({
  wordwrap: 130,
});
const fs = require("fs");

const html = fs.readFileSync("./HTMLTestFile.htm", {
  encoding: "utf8",
  flag: "r",
});

const texts = html.map(convert);
console.log(texts.join("\n"));
// Hello World!
// こんにちは世界！
// Привет, мир!

const stop = Date.now();
console.log(`Time Taken to execute = ${(stop - start) / 1000} seconds`);

// console.log(org);
// histogram program, word frequency
