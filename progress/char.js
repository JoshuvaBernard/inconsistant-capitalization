function finder(text, word) {
  const text_arr = text.split(/[,\s\n]/);
  let size = 0;
  for (let i = 0; i < text_arr.length; i++) {
    if (text_arr[i] == word) {
      break;
    }
    size = size + text_arr[i].length;
    size++;
  }
  console.log(size);
}
finder(
  `Kalnay, E., Kanamitsu, M., Kistler, R.,
Collins, W., Deaven, D., Gandin, L., Iredell, M., Saha, S., White, G., Woollen,
J., Zhu, Y., Chelliah, M., Ebisuzaki, W., Higgins, W., Janowiak, J., Mo, K.C.,
Ropelewski, C., Wang, J., Leetmaa, A., ... Joseph, D. (1996) The NCEP/NCAR
40-year reanalysis project. Bulletin of the American Meteorological Society
77(3), 437-471.`,
  "Kanamitsu"
);

let text = `Kalnay, E., Kanamitsu, M., Kistler, R.,
Collins, W., Deaven, D., Gandin, L., Iredell, M., Saha, S., White, G., Woollen,
J., Zhu, Y., Chelliah, M., Ebisuzaki, W., Higgins, W., Janowiak, J., Mo, K.C.,
Ropelewski, C., Wang, J., Leetmaa, A., ... Joseph, D. (1996) The NCEP/NCAR
40-year reanalysis project. Bulletin of the American Meteorological Society
77(3), 437-471.`;
let result = text.indexOf("Kanamitsu");
console.log(result);
