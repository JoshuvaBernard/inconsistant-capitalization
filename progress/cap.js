function caseCheck(current, previous) {
  if (current === previous) {
    return 1;
  } else if (current.toLowerCase() === previous.toLowerCase()) {
    return 2;
  } else {
    return 0;
  }
}

console.log(caseCheck("A", "a"));
// const str1 = "microsoft.com";
// const str2 = "microsoft.com";
// console.log(str1.localeCompare(str2));
