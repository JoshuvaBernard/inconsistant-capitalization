const arr = ["zero", "one", "two"];

const obj1 = Object.assign({}, arr);
console.log(obj1[0]); // 👉️ {0: 'zero', 1: 'one', 2: 'two'}
